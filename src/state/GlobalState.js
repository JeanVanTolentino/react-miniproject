import React, { createContext, useState } from "react";

export const GlobalContext = createContext();

function GlobalState({ children }) {
 const [allMovies, setAllMovies] = useState([]);
 const [featuredMovies, setFeaturedMovies] = useState([]);
 const [isHomeLoading, setIsHomeLoading] = useState(true);

 return (
  <GlobalContext.Provider
   value={{
    allMovies,
    setAllMovies,
    featuredMovies,
    setFeaturedMovies,
    isHomeLoading,
    setIsHomeLoading,
   }}
  >
   {children}
  </GlobalContext.Provider>
 );
}

export default GlobalState;
