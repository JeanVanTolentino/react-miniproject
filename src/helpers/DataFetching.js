import axios from "axios";

export async function getAllMovies() {
 const allMovies = await axios({
  method: "GET",
  url: "https://studio-ghibli-films-api.herokuapp.com/api",
 }).catch(() => {
  console.log("API not available");
 });
 //  const delay = (delayInms) => {
 //   return new Promise((resolve) => setTimeout(resolve, delayInms));
 //  };

 //  await delay(2000);

 return allMovies;
}
