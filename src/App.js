import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import LandingPage from "./components/LandingPage/LandingPage";
import NavBar from "./components/NavBar/NavBar";
import ListPage from "./components/ListPage/ListPage";
import { useContext, useEffect } from "react";
import { GlobalContext } from "./state/GlobalState";
import { getAllMovies } from "./helpers/DataFetching";

function App() {
 const { setAllMovies, setFeaturedMovies, setIsHomeLoading } =
  useContext(GlobalContext);

 useEffect(() => {
  async function getMovies() {
   const movies = await getAllMovies();

   const arrMovies = Object.values(movies.data);

   setAllMovies([arrMovies]);
   setFeaturedMovies([arrMovies[10], arrMovies[6], arrMovies[5]]);
   setIsHomeLoading(false);
  }

  getMovies();
 }, []);

 return (
  <BrowserRouter>
   <NavBar />
   <section className="body">
    <Routes>
     <Route path="/" element={<LandingPage />}></Route>
     <Route path="movies-list" element={<ListPage />}></Route>
    </Routes>
   </section>
  </BrowserRouter>
 );
}

export default App;
