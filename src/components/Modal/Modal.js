import React, { useContext } from "react";
import "./Modal.css";
import { GlobalContext } from "../../state/GlobalState";

function DetailsModal({ clickedMovie, isModalOpen, setIsModalOpen }) {
 const { featuredMovies, setFeaturedMovies } = useContext(GlobalContext);
 const isAdded = featuredMovies.find(
  (movie) => movie.title === clickedMovie.title
 );

 return (
  <section
   className={`react-modal-parent ${isModalOpen ? "" : "react-modal-remove"}`}
  >
   <div className="react-modal-screen">
    <img src={clickedMovie?.poster} alt={clickedMovie?.title} />
    <div className="list-page-details">
     <h3>
      Title: <span className="label-value">{clickedMovie.title}</span>
     </h3>
     <h3>
      Genre: <span className="label-value">{clickedMovie.genre}</span>
     </h3>
     <h3>
      Rating: <span className="label-value">{clickedMovie.rating}</span>
     </h3>
     <h3>
      Release: <span className="label-value">{clickedMovie.release}</span>
     </h3>
     <h3>
      Director: <span className="label-value">{clickedMovie.director}</span>
     </h3>
     <span>
      <h3>Synposis:</h3>
      <p
       style={{
        color: "black",
        fontWeight: "bold",
        lineHeight: "20px",
        textAlign: "justify",
       }}
      >
       {clickedMovie.synopsis}
      </p>
     </span>
     <h3>Reviews:</h3>
     <div className="details-reviews-section">
      <div>Rotten Tomatoes: {clickedMovie.reviews?.rottenTomatoes}</div>
      <div>imdb: {clickedMovie.reviews?.imdb}</div>
     </div>
     <button
      className={`add-featured-button ${
       isAdded ? "" : "add-featured-button-addable"
      }`}
      onClick={() => {
       if (isAdded) {
        return;
       }

       const newList = [...featuredMovies, clickedMovie];
       if (newList.length > 6) {
        newList.shift();
       }

       setFeaturedMovies(newList);
      }}
     >
      {isAdded ? "Already in Featured" : "Add to Featured"}
     </button>
    </div>
   </div>
   <div
    className="react-modal-blocker"
    onClick={() => {
     setIsModalOpen(false);
    }}
   ></div>
  </section>
 );
}

export default DetailsModal;
