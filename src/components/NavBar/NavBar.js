import React from "react";
import "./NavBar.css";
import { Link } from "react-router-dom";

function NavBar() {
 return (
  <section className="navbar">
   <Link to="/">
    <img src="./favicon.png" alt="totoro" className="navbar-icon" />
   </Link>
  </section>
 );
}

export default NavBar;
