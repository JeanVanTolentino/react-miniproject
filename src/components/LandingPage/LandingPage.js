import React, { useContext, useEffect } from "react";
import "./LandingPage.css";
import { GlobalContext } from "../../state/GlobalState";
import FeaturedSection from "./FeaturedSection";
import About from "./About";
import { getAllMovies } from "../../helpers/DataFetching";
import Loading from "../Animation/Loading";

function LandingPage() {
 const { featuredMovies } = useContext(GlobalContext);

 return (
  <section className="landing-page-screen">
   <FeaturedSection featuredMovies={featuredMovies} />
   <About />
  </section>
 );
}

export default LandingPage;
