import React from "react";
import { Link } from "react-router-dom";
import "./About.css";

function About() {
 return (
  <section className="landing-page-see-more">
   <img src="./favicon.png" alt="totoro.png" />
   <div>
    <h1>Welcome to Studio Ghibli</h1>
    <Link className="see-all-button" to="/movies-list">
     See all available movies
    </Link>
   </div>
  </section>
 );
}

export default About;
