import React, { useState, useContext, useEffect } from "react";
import "./FeaturedSection.css";
import {
 BsFillArrowLeftCircleFill,
 BsFillArrowRightCircleFill,
} from "react-icons/bs";
import Loading from "../Animation/Loading";
import { GlobalContext } from "../../state/GlobalState";

function FeaturedSection({ featuredMovies }) {
 let isMobileView = window.innerWidth <= 869;
 let marginIncrement = isMobileView ? 100 : 33.33;
 const { isHomeLoading } = useContext(GlobalContext);
 const [carouselPosition, setCarouselPosition] = useState(0);
 const [carouselCardPosition, setCarouselCardPosition] = useState(0);
 const [currentPosition, setCurrentPosition] = useState(1);

 function createCarouselDots() {
  const parts = [];

  for (let i = 0; carouselCardPosition > i; i++) {
   parts.push(
    <div
     className="carousel-dots"
     key={i}
     style={i + 1 === currentPosition ? { background: "white" } : {}}
    ></div>
   );
  }

  return parts;
 }

 useEffect(() => {
  window.addEventListener("resize", (e) => {
   let newState = e.currentTarget.innerWidth <= 869;

   if (isMobileView !== newState) {
    isMobileView = newState;
    setCarouselCardPosition(
     isMobileView ? featuredMovies.length : featuredMovies.length - 2
    );
    setCarouselPosition(0);
    setCurrentPosition(1);
   }
  });

  return () => {
   window.removeEventListener("resize", (e) => {
    let newState = e.currentTarget.width <= 869;

    if (isMobileView !== newState) {
     isMobileView = newState;
     setCarouselCardPosition(
      isMobileView ? featuredMovies.length : featuredMovies.length - 2
     );
    }
   });
  };
 }, []);

 useEffect(() => {
  setCarouselCardPosition(
   isMobileView ? featuredMovies.length : featuredMovies.length - 2
  );
 });

 return (
  <section className="landing-page-featured-parent">
   <h1>Featured Movies</h1>
   <div className="carousel-circle-components">{createCarouselDots()}</div>
   {isHomeLoading ? (
    <Loading width="200px" />
   ) : (
    <div className="landing-page-buttons-wrapper">
     <div
      className="carousel-left-button carousel-button-hover-effects"
      style={currentPosition === 1 ? { display: "none" } : {}}
      onClick={() => {
       setCurrentPosition(currentPosition - 1);
       setCarouselPosition(carouselPosition + marginIncrement);
      }}
     >
      <BsFillArrowLeftCircleFill className="carousel-logo" />
     </div>
     <div
      className="carousel-right-button carousel-button-hover-effects"
      style={
       currentPosition === carouselCardPosition || carouselCardPosition < 0
        ? { display: "none" }
        : {}
      }
      onClick={() => {
       setCurrentPosition(currentPosition + 1);
       setCarouselPosition(carouselPosition - marginIncrement);
      }}
     >
      <BsFillArrowRightCircleFill className="carousel-logo" />
     </div>
     <section className="landing-page-featured">
      {featuredMovies.map((featuredMovie, index) => {
       const { title, poster, genre, rating, release } = featuredMovie;
       let styling;

       switch (index) {
        case 0:
         styling = { marginLeft: `${carouselPosition}%` };
         break;
       }

       return (
        <div
         className="featured-tile-wrapper"
         key={title}
         style={{ ...styling, transition: "margin-left 0.5s" }}
        >
         <div key={title} className="featured-tile">
          <img src={poster} alt={featuredMovie.title} />
          <div className="featured-tile-label">
           <h3>{title}</h3>
           <h3>{genre}</h3>
           <h3>{rating}</h3>
           <h3>{release}</h3>
          </div>
         </div>
        </div>
       );
      })}
     </section>
    </div>
   )}
  </section>
 );
}

export default FeaturedSection;
