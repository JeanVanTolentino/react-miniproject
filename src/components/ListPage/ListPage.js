import React, { useEffect, useRef, useState } from "react";
import "./ListPage.css";
import { getAllMovies } from "../../helpers/DataFetching";
import DetailsModal from "../Modal/Modal";
import { FaSearch } from "react-icons/fa";
import Loading from "../Animation/Loading";

function ListPage() {
 let [paginatedMovies, setPaginatedMovies] = useState([]);
 let [allMoviesCollection, setAllMoviesCollection] = useState([]);
 let [isContinueLoading, setIsContinueLoading] = useState(true);
 let [loadedItems, setLoadedItems] = useState(10);
 let [isModalOpen, setIsModalOpen] = useState(false);
 let [clickedMovie, setClickedMovie] = useState({});

 const dropdownType = useRef(null);
 const dropdownOrder = useRef(null);

 useEffect(() => {
  async function getMovies() {
   const allMovies = await getAllMovies();
   const allMoviesArr = Object.values(allMovies.data);
   const paginatedMoviesArr = allMoviesArr.slice(0, loadedItems);

   if (allMoviesArr.length === paginatedMoviesArr.length) {
    setIsContinueLoading(false);
   }
   setAllMoviesCollection(allMoviesArr);
   setPaginatedMovies(paginatedMoviesArr);
  }

  getMovies();
 }, []);

 return (
  <>
   <div className="filter-results-main-wrapper">
    <div className="filter-results-section">
     <p>Filter Results:</p>
     <div className="filter-dropdowns-section">
      <select ref={dropdownType} name="filter-type" className="filter-dropdown">
       <option value="Date released">Date released</option>
       <option value="Alphabetical">Alphabetical</option>
       <option value="imdb rating">imdb rating</option>
       <option value="rotten tomatoes rating">rotten tomatoes rating</option>
      </select>
      <select
       ref={dropdownOrder}
       name="filter-order"
       className="filter-dropdown"
      >
       <option value="Ascending">Ascending</option>
       <option value="Descending">Descending</option>
      </select>
      <button
       className="filter-button-dropdown"
       onClick={() => {
        let filteredMovies;

        filteredMovies = allMoviesCollection.sort((a, b) => {
         let x;
         let y;

         if (dropdownType.current.value === "Alphabetical") {
          x = a.title.toLowerCase();
          y = b.title.toLowerCase();
         }

         if (dropdownType.current.value === "Date released") {
          x = new Date(a.release);
          y = new Date(b.release);
         }

         if (dropdownType.current.value === "imdb rating") {
          x = parseFloat(a.reviews.imdb.split("/")[0]);
          y = parseFloat(b.reviews.imdb.split("/")[0]);
         }

         if (dropdownType.current.value === "rotten tomatoes rating") {
          x = parseFloat(a.reviews.rottenTomatoes.replace("%", ""));
          y = parseFloat(b.reviews.rottenTomatoes.replace("%", ""));
         }
         if (dropdownOrder.current.value === "Ascending") {
          if (x < y) {
           return -1;
          }
          if (x > y) {
           return 1;
          }
          return 0;
         } else {
          if (x > y) {
           return -1;
          }
          if (x < y) {
           return 1;
          }
          return 0;
         }
        });

        setPaginatedMovies(filteredMovies.slice(0, 10));
        setLoadedItems(10);
        setIsContinueLoading(true);
       }}
      >
       Filter
      </button>
     </div>
    </div>
    <div className="filter-search-section">
     <form
      style={{ paddingRight: "5px" }}
      onSubmit={(e) => {
       e.preventDefault();
       const { search } = Object.fromEntries(new FormData(e.currentTarget));

       setPaginatedMovies(
        allMoviesCollection.filter((x) => {
         return x.title.toLowerCase().includes(search.toLowerCase());
        })
       );
       setIsContinueLoading(false);
      }}
     >
      <input type="text" name="search" className=""></input>
     </form>
     <FaSearch />
    </div>
   </div>
   {allMoviesCollection.length === 0 ? (
    <Loading width="200px" />
   ) : (
    <section className="list-page-screen">
     {paginatedMovies.map((movie) => {
      return (
       <div
        className="list-page-tile"
        key={movie.title}
        onClick={() => {
         setIsModalOpen(true);
         setClickedMovie(movie);
        }}
       >
        <div className="list-page-tile-label">
         <h2>{movie.title}</h2>
        </div>

        <img src={movie.poster} alt={movie.title} />
       </div>
      );
     })}
     <div className="list-page-tile tile-zoom-in">
      <button
       className="list-page-see-more"
       onClick={() => {
        if (isContinueLoading) {
         const newLoadedItems = loadedItems + 10;
         const newPaginatedMovies = allMoviesCollection.slice(
          0,
          newLoadedItems
         );
         if (newPaginatedMovies.length === allMoviesCollection.length) {
          setIsContinueLoading(false);
         }
         setLoadedItems(newLoadedItems);
         setPaginatedMovies(newPaginatedMovies);
        }
       }}
      >
       <img src="./favicon.png" alt="totoro" />
       <h2>{isContinueLoading ? "Keep Looking!" : "That's Everything"}</h2>
      </button>
     </div>
     <DetailsModal
      isModalOpen={isModalOpen}
      setIsModalOpen={setIsModalOpen}
      clickedMovie={clickedMovie}
      setClickedMovie={setClickedMovie}
     ></DetailsModal>
    </section>
   )}
  </>
 );
}

export default ListPage;
