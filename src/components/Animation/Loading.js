import React from "react";
import "./Loading.css";
import umbrella from "../../images/umbrella.png";
import { TbDropletFilled } from "react-icons/tb";

function Loading({ width }) {
 function createDroplets() {
  const droplets = [];

  for (let i = 0; i < 10; i++) {
   let animation;

   switch (i + 1) {
    case 1:
     animation = "firstdroplet 0.5s infinite linear -0.5s";
     break;
    case 2:
     animation = "firstdroplet 0.5s infinite linear -0.3s";
     break;
    case 3:
     animation = "firstdroplet 0.5s infinite linear -0.9s";
     break;
    case 4:
     animation = "firstdroplet 0.5s infinite linear -1.1s";
     break;
    case 5:
     animation = "firstdroplet 0.5s infinite linear -0.6s";
     break;
    case 6:
     animation = "firstdroplet 0.5s infinite linear -0.4s";
     break;
    case 7:
     animation = "firstdroplet 0.5s infinite linear -1s";
     break;
    case 8:
     animation = "firstdroplet 0.5s infinite linear -0.14s";
     break;
    case 9:
     animation = "firstdroplet 0.5s infinite linear -0.34s";
     break;
    case 10:
     animation = "firstdroplet 0.5s infinite linear -0.9s";
     break;
   }

   droplets.push(
    <TbDropletFilled
     className="loading-animation-droplet"
     key={i}
     style={{ left: `${(i + 1) * 10}%`, animation }}
    />
   );
  }

  return droplets;
 }

 return (
  <div className="loading-animation-wrapper">
   <div className="loading-animation-container">
    {createDroplets()}
    <img src={umbrella} className="loading-animation-image" style={{ width }} />
    <h2 style={{ textAlign: "center", lineHeight: "40px" }}>Please Wait</h2>
   </div>
  </div>
 );
}

export default Loading;
